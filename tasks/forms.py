from django.forms import ModelForm
from tasks.models import Task


# Create TaskForm form for creating new tasks
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
