from django.forms import ModelForm
from projects.models import Project


# Create ProjectForm form for creating new projects
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
